package org.olamy.simplehttpserver.core;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.apache.commons.lang3.StringUtils;
import org.olamy.simplehttpserver.HttpServerConfiguration;
import org.olamy.simplehttpserver.SimpleHttpServerException;
import org.olamy.simplehttpserver.handler.RequestHandler;
import org.olamy.simplehttpserver.http.HttpHeaders;
import org.olamy.simplehttpserver.http.HttpRequest;
import org.olamy.simplehttpserver.http.HttpResponse;
import org.olamy.simplehttpserver.http.ResponseStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author Olivier Lamy
 */
public abstract class AbstractHttpRequestProcessor
{

    private HttpServerConfiguration httpServerConfiguration;

    protected Logger log = LoggerFactory.getLogger( getClass() );

    protected AbstractHttpRequestProcessor( HttpServerConfiguration httpServerConfiguration )
    {
        this.httpServerConfiguration = httpServerConfiguration;
    }

    public HttpServerConfiguration getHttpServerConfiguration()
    {
        return httpServerConfiguration;
    }

    public void setHttpServerConfiguration( HttpServerConfiguration httpServerConfiguration )
    {
        this.httpServerConfiguration = httpServerConfiguration;
    }

    /**
     *
     * @param httpRequest
     * @return header value of Connection header or null if not found
     */
    protected String findKeepAliveHeader( HttpRequest httpRequest )
    {

        List<String> headerValues = httpRequest.getHeaderValues( HttpHeaders.CONNECTION );
        if ( headerValues.isEmpty() )
        {
            return null;
        }

        for ( String headerValue : headerValues )
        {
            return headerValue;

        }

        return null;
    }

    protected boolean isKeepAlive( HttpRequest httpRequest )
    {
        return !StringUtils.equals( findKeepAliveHeader( httpRequest ), "close" );
    }

    public HttpResponse buildResponse( HttpRequest request, HttpResponse response )
        throws SimpleHttpServerException
    {
        response.setProtocolName( request.getProtocolName() );
        response.setProtocolVersion( request.getProtocolVersion() );

        for ( RequestHandler requestHandler : this.getHttpServerConfiguration().getRequestHandlers() )
        {
            if ( requestHandler.canServe( request ) )
            {
                requestHandler.handle( request, response );
                return response;
            }
        }
        // FIXME maybe something else to use as status code
        throw new SimpleHttpServerException().setResponseStatus( ResponseStatus.METHOD_NOT_ALLOWED );
    }
}
