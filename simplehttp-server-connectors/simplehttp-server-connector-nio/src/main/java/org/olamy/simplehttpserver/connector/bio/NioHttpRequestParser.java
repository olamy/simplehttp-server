package org.olamy.simplehttpserver.connector.bio;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.olamy.simplehttpserver.InvalidRequestException;
import org.olamy.simplehttpserver.core.parser.AbstractHttpRequestParser;
import org.olamy.simplehttpserver.http.HttpContentConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

/**
 * @author Olivier Lamy
 */
public class NioHttpRequestParser
    extends AbstractHttpRequestParser
{
    private Logger log = LoggerFactory.getLogger( getClass() );

    private char lastChar;

    private StringBuilder accumulatedContent = new StringBuilder();

    private boolean requestConsumed = false;

    private String lastLine;

    public NioHttpRequestParser()
    {
        super();
    }

    public void consumeData( ByteBuffer dataBuffer )
        throws InvalidRequestException
    {
        CharBuffer buffer = toCharBuffer( dataBuffer );
        while ( buffer.hasRemaining() )
        {
            char c = buffer.get();
            if ( log.isTraceEnabled() )
            {
                log.trace( "current char '{}'", c );
            }
            switch ( c )
            {
                case HttpContentConstants.CR:
                    if ( log.isTraceEnabled() )
                    {
                        log.trace( "read: \\r" );
                    }
                    break;
                case HttpContentConstants.LF:
                    if ( log.isTraceEnabled() )
                    {
                        log.trace( "read: \\n" );
                    }
                    if ( lastChar == HttpContentConstants.CR && accumulatedContent.length() == 0 )
                    {
                        requestConsumed = true;
                        return;
                    }

                    log.debug( "accumuledContent: {}", accumulatedContent );
                    parseLine( accumulatedContent.toString() );
                    lastLine = accumulatedContent.toString();
                    accumulatedContent = new StringBuilder();

                    break;
                default:
                    if ( log.isTraceEnabled() )
                    {
                        log.trace( "read: {}", c );
                    }
                    accumulatedContent.append( c );
            }

            this.lastChar = c;
        }
    }

    private CharBuffer toCharBuffer( ByteBuffer byteBuffer )
    {
        return Charset.forName( "UTF-8" ).decode( byteBuffer );
    }

    public boolean isRequestConsumed()
    {
        return requestConsumed;
    }
}
