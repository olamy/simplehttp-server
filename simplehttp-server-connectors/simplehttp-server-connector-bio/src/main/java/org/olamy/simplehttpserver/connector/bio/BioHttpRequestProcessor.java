package org.olamy.simplehttpserver.connector.bio;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.olamy.simplehttpserver.HttpServerConfiguration;
import org.olamy.simplehttpserver.InvalidRequestException;
import org.olamy.simplehttpserver.SimpleHttpServerException;
import org.olamy.simplehttpserver.core.AbstractHttpRequestProcessor;
import org.olamy.simplehttpserver.http.HttpContentConstants;
import org.olamy.simplehttpserver.http.HttpHeaders;
import org.olamy.simplehttpserver.http.HttpRequest;
import org.olamy.simplehttpserver.http.HttpResponse;
import org.olamy.simplehttpserver.http.HttpResponseBuilder;
import org.olamy.simplehttpserver.http.ResponseBodyWriter;
import org.olamy.simplehttpserver.http.ResponseStatus;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.List;
import java.util.Map;

/**
 * @author Olivier Lamy
 */
public class BioHttpRequestProcessor
    extends AbstractHttpRequestProcessor
{
    private Socket socket;

    public BioHttpRequestProcessor( Socket socket, HttpServerConfiguration httpServerConfiguration )
    {
        super( httpServerConfiguration );
        this.socket = socket;
    }


    public void processRequest()
        throws IOException
    {
        BioHttpRequestParser httpRequestParser = new BioHttpRequestParser();

        HttpResponse httpResponse = HttpResponseBuilder.newInstance( getHttpServerConfiguration() );

        HttpRequest httpRequest = null;

        try
        {
            httpRequest = httpRequestParser.parse( new BufferedInputStream( this.socket.getInputStream() ) );

            httpResponse = buildResponse( httpRequest, httpResponse );
        }
        catch ( InvalidRequestException e )
        {
            // use a response with default value http and 1.1
            // and configure status to return
            httpResponse.setStatus( ResponseStatus.BAD_REQUEST );
        }
        catch ( SimpleHttpServerException e )
        {
            httpResponse.setStatus(
                e.getResponseStatus() == null ? ResponseStatus.INTERNAL_SERVER_ERROR : e.getResponseStatus() );
        }
        finally
        {
            handleKeepAlive( httpRequest, httpResponse );
        }
        writeResponse( httpResponse );

    }

    public void writeResponse( HttpResponse httpResponse )
        throws IOException
    {
        //DataOutputStream dataOutputStream = new DataOutputStream( socket.getOutputStream() );

        OutputStream dataOutputStream = socket.getOutputStream();

        //write status line "HTTP/1.1 200 OK
        dataOutputStream.write(
            ( httpResponse.getProtocolName() + '/' + httpResponse.getProtocolVersion() ).getBytes() );
        dataOutputStream.write( ( ' ' + Integer.toString( httpResponse.getStatus().getCode() ) ).getBytes() );
        dataOutputStream.write( ( ' ' + httpResponse.getStatus().getText() ).getBytes() );

        dataOutputStream.write( HttpContentConstants.CR );
        dataOutputStream.write( HttpContentConstants.LF );

        dataOutputStream.write( ( HttpHeaders.CONTENT_LENGTH + HttpContentConstants.HEADER_NAME_SEPARATOR + ' '
            + httpResponse.getContentLength() ).getBytes() );

        dataOutputStream.write( HttpContentConstants.CR );
        dataOutputStream.write( HttpContentConstants.LF );

        if ( httpResponse.getContentType() != null )
        {
            dataOutputStream.write( ( HttpHeaders.CONTENT_TYPE + HttpContentConstants.HEADER_NAME_SEPARATOR + ' '
                + httpResponse.getContentType() ).getBytes() );

            dataOutputStream.write( HttpContentConstants.CR );
            dataOutputStream.write( HttpContentConstants.LF );
        }

        for ( Map.Entry<String, List<String>> header : httpResponse.getHeaders().entrySet() )
        {

            StringBuilder headerLine = new StringBuilder( header.getKey() ).append( ':' ).append( ' ' );
            int counter = 0;
            for ( String headerValue : header.getValue() )
            {
                headerLine.append( ( counter++ > 0 ? ", " : "" ) ).append( headerValue );
            }

            dataOutputStream.write( headerLine.toString().getBytes() );
            dataOutputStream.write( HttpContentConstants.CR );
            dataOutputStream.write( HttpContentConstants.LF );

        }

        dataOutputStream.write( HttpContentConstants.CR );
        dataOutputStream.write( HttpContentConstants.LF );

        ResponseBodyWriter responseBodyWriter = httpResponse.getResponseBodyWriter();

        if ( responseBodyWriter != null )
        {
            responseBodyWriter.writeBody( dataOutputStream );
        }

        dataOutputStream.flush();
        dataOutputStream.close();
    }

    private void handleKeepAlive( HttpRequest httpRequest, HttpResponse httpResponse )
        throws IOException
    {
        if ( httpRequest == null || httpResponse == null )
        {
            return;
        }

        httpResponse.addHeaderValue( HttpHeaders.CONNECTION, "Close" );
        /*
        if ( HttpContentConstants.HTTP_1_1.equals( httpRequest.getProtocolVersion() ) || isKeepAlive( httpRequest ) )
        {
            this.socket.setKeepAlive( true );
            this.socket.setSoTimeout( getHttpServerConfiguration().getKeepAliveTimeout() );

            httpResponse.addHeaderValue( HttpHeaders.KEEP_ALIVE, format( "timeout=%s", socket.getSoTimeout() / 1000 ) );
            httpResponse.addHeaderValue( HttpHeaders.CONNECTION, HttpHeaders.KEEP_ALIVE );

        }*/

    }


}
