package org.olamy.simplehttpserver.connector.bio;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.olamy.simplehttpserver.HttpServerConfiguration;
import org.olamy.simplehttpserver.SimpleHttpServerException;
import org.olamy.simplehttpserver.connector.ConnectorStatus;
import org.olamy.simplehttpserver.connector.HttpConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ServerSocketFactory;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author Olivier Lamy
 */
public class BioHttpConnector
    implements HttpConnector
{

    private Logger log = LoggerFactory.getLogger( getClass() );

    private HttpServerConfiguration httpServerConfiguration;

    private ServerSocket serverSocket;

    private ServerSocketFactory serverSocketFactory;

    private int localPort;

    private final AtomicReference<ConnectorStatus> connectorStatus = new AtomicReference<ConnectorStatus>();

    private Thread socketListenThread;

    private ExecutorService connectionExecutorService;

    public void initialize( HttpServerConfiguration httpServerConfiguration )
        throws SimpleHttpServerException
    {
        this.httpServerConfiguration = httpServerConfiguration;

        try
        {

            serverSocketFactory = ServerSocketFactory.getDefault();

            serverSocket = serverSocketFactory.createServerSocket( this.httpServerConfiguration.getPort(), 10,
                                                                   InetAddress.getByName(
                                                                       this.httpServerConfiguration.getHost() ) );

            connectionExecutorService = Executors.newFixedThreadPool( httpServerConfiguration.getMaxThreads() );

            localPort = serverSocket.getLocalPort();

            log.debug( "local port {}", localPort );
        }
        catch ( IOException e )
        {
            throw new SimpleHttpServerException( e.getMessage(), e );
        }


    }

    public void connect()
        throws SimpleHttpServerException
    {

        Runnable socketListenRunnable = new Runnable()
        {
            public void run()
            {
                while ( !serverSocket.isClosed() )
                {
                    try
                    {

                        final Socket socket = serverSocket.accept();

                        connectionExecutorService.submit( new Callable<Void>()
                        {
                            public Void call()
                                throws Exception
                            {
                                BioHttpRequestProcessor httpRequestProcessor =
                                    new BioHttpRequestProcessor( socket, httpServerConfiguration );

                                httpRequestProcessor.processRequest();
                                return null;
                            }
                        } );


                    }
                    catch ( IOException e )
                    {
                        if ( !serverSocket.isClosed() )
                        {
                            throw new RuntimeException( e.getMessage(), e );
                        }
                    }
                }

            }
        };

        socketListenThread = new Thread( socketListenRunnable );

        socketListenThread.start();
        log.debug( "thread started" );

    }

    public int getLocalPort()
    {
        return localPort;
    }

    public void shutdown()
        throws SimpleHttpServerException
    {
        this.connectorStatus.set( ConnectorStatus.STOPPED );
        connectionExecutorService.shutdownNow();
        socketListenThread.interrupt();
        try
        {
            serverSocket.close();
        }
        catch ( IOException e )
        {
            throw new RuntimeException( e.getMessage(), e );
        }

    }
}
