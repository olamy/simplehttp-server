package org.olamy.simplehttpserver.connector.bio;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.olamy.simplehttpserver.InvalidRequestException;
import org.olamy.simplehttpserver.core.parser.AbstractHttpRequestParser;
import org.olamy.simplehttpserver.http.HttpContentConstants;
import org.olamy.simplehttpserver.http.HttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Olivier Lamy
 */
public class BioHttpRequestParser
    extends AbstractHttpRequestParser
{

    private Logger log = LoggerFactory.getLogger( getClass() );

    private char lastChar;

    private StringBuilder accumulatedContent = new StringBuilder();


    public BioHttpRequestParser()
    {
        super();
    }


    public HttpRequest parse( InputStream is )
        throws IOException, InvalidRequestException
    {
        int read = is.read();
        while ( read != -1 )
        {
            char c = (char) read;

            switch ( c )
            {
                case HttpContentConstants.CR:
                    log.trace( "read: \\r" );
                    if ( lastChar == HttpContentConstants.LF && accumulatedContent.length() == 0 )
                    {
                        return getHttpRequest();
                    }
                    break;
                case HttpContentConstants.LF:
                    log.trace( "read: \\n" );
                    if ( lastChar == HttpContentConstants.CR && accumulatedContent.length() == 1 )
                    {
                        return getHttpRequest();
                    }

                    log.trace( "accumuledContent: {}", accumulatedContent );
                    parseLine( accumulatedContent.toString() );
                    accumulatedContent = new StringBuilder();

                    break;
                default:
                    log.trace( "read: {}", c );
                    accumulatedContent.append( c );
            }
            read = is.read();
            this.lastChar = c;
        }
        log.debug( "httpRequest: {}", getHttpRequest().toString() );
        return this.getHttpRequest();
    }


}
