simplehttp-server
=================
a very simple file http server written in java

Java 1.6 and maven 3.0.x are needed.
Just run: mvn clean install

Then: cd simplehttp-server-launcher/target/ && unzip simplehttp-server-launcher-1.0-SNAPSHOT.zip
      && cd simplehttp-server-launcher-1.0-SNAPSHOT/bin/ && ./simpleserver

Hit your browser on http://localhost:8080/

For more options use: ./simpleserver -h

There is 2 implementations of http connector:
* BIO: Blocking IO
* NIO: Native IO

NOTE: currently only GET method is supported (and BIO http connector doesn't support keep alive)


