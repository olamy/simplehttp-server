package org.olamy.simplehttpserver.http;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/**
 * @author Olivier Lamy
 */
public enum ResponseStatus
{

    /**
     * Ok status.
     */
    OK( 200, "OK" ),

    /**
     * Bad status.
     */
    BAD_REQUEST( 400, "Bad Request" ),
    /**
     * Forbidden status.
     */
    FORBIDDEN( 403, "Forbidden" ),
    /**
     * Not Found status.
     */
    NOT_FOUND( 404, "Not Found" ),
    /**
     * Method Not Allowed status.
     */
    METHOD_NOT_ALLOWED( 405, "Method Not Allowed" ),
    /**
     * Internal Server Error status.
     */
    INTERNAL_SERVER_ERROR( 500, "Internal Server Error" );

    /**
     * HTTP status code.
     */
    private final int code;

    /**
     * The HTTP status text.
     */
    private final String text;

    /**
     * new HTTP status.
     *
     * @param code the HTTP status code.
     * @param text the content of HTTP status text.
     */
    ResponseStatus( int code, String text )
    {
        this.code = code;
        this.text = text;
    }

    /**
     * @return the HTTP status code.
     */
    public int getCode()
    {
        return code;
    }

    /**
     * @return the HTTP status text.
     */
    public String getText()
    {
        return text;
    }

}
