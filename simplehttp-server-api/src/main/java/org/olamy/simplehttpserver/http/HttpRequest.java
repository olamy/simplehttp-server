package org.olamy.simplehttpserver.http;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Olivier Lamy
 */
public class HttpRequest
{

    private String host;

    private HttpMethod method;

    private String path;

    private String protocolName;

    private String protocolVersion;

    private long contentLength = -1;

    private String accept;

    private ByteBuffer requestBody;

    /**
     * map representing headers: key is header name and List of possible values.
     */
    private Map<String, List<String>> headers = new HashMap<String, List<String>>();

    public String getHost()
    {
        return host;
    }

    public void setHost( String host )
    {
        this.host = host;
    }


    public HttpMethod getMethod()
    {
        return method;
    }

    public void setMethod( HttpMethod method )
    {
        this.method = method;
    }

    public String getPath()
    {
        return path;
    }

    public void setPath( String path )
    {
        this.path = path;
    }

    public String getProtocolName()
    {
        return protocolName;
    }

    public void setProtocolName( String protocolName )
    {
        this.protocolName = protocolName;
    }

    public String getProtocolVersion()
    {
        return protocolVersion;
    }

    public void setProtocolVersion( String protocolVersion )
    {
        this.protocolVersion = protocolVersion;
    }

    public long getContentLength()
    {
        return contentLength;
    }

    public void setContentLength( long contentLength )
    {
        this.contentLength = contentLength;
    }

    public ByteBuffer getRequestBody()
    {
        return requestBody;
    }

    public void setRequestBody( ByteBuffer requestBody )
    {
        this.requestBody = requestBody;
    }

    public Map<String, List<String>> getHeaders()
    {
        return headers;
    }

    public void setHeaders( Map<String, List<String>> headers )
    {
        this.headers = headers;
    }

    public List<String> getHeaderValues( String headerName )
    {
        List<String> headerValues = this.headers.get( headerName );
        return headerValues == null ? Collections.<String>emptyList() : headerValues;
    }

    public void addHeaderValue( String headerName, String headerValue )
    {
        List<String> headerValues = this.headers.get( headerName );
        if ( headerValues == null )
        {
            headerValues = new ArrayList<String>();
            this.headers.put( headerName, headerValues );
        }
        headerValues.add( headerValue );
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder();
        sb.append( "HttpRequest" );
        sb.append( "{host='" ).append( host ).append( '\'' );
        sb.append( ", method=" ).append( method );
        sb.append( ", path='" ).append( path ).append( '\'' );
        sb.append( ", protocolName='" ).append( protocolName ).append( '\'' );
        sb.append( ", protocolVersion='" ).append( protocolVersion ).append( '\'' );
        sb.append( ", contentLength=" ).append( contentLength );
        sb.append( ", accept='" ).append( accept ).append( '\'' );
        sb.append( ", requestBody=" ).append( requestBody );
        sb.append( ", headers=" ).append( headers );
        sb.append( '}' );
        return sb.toString();
    }
}
